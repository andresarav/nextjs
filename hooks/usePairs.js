import { useEffect, useState } from 'react'
import { get_pairs } from '../lib/pairs'

export const usePairs = () => {

    const [ pairs, setPairs ] = useState()
    // const [ currentPair, setCurrentPair ] = useState()
    // const [ localCurrency, setLocalCurrency ] = useState()

    const initializeOrUpdate = async(current_pair) => {
      // let local = await get_local_currency('colombia')
      let pairs = await get_pairs()
      // console.log('<===== initialized ===>::: ', pairs, local)
      // debugger
      // if(!local || !pairs){return false}
      // let local_currency = local.currency.toUpperCase()
      // if(current_pair){
      //   setCurrentPair(pairs.find(pair=>pair.buy_pair === current_pair.buy_pair))
      // }else{
      //   setCurrentPair(pairs.find(pair=>pair.buy_pair === `BTC/${local_currency}`))
      // }
      setPairs(pairs)
      // setLocalCurrency(local)
    }

    // const changePair = new_pair => {
    //   setCurrentPair(pairs.find(pair=>pair.buy_pair === new_pair.buy_pair))
    // }

    useEffect(()=>{
      initializeOrUpdate()
    }, [])

  return [ pairs ]
}
