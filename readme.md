Next.js automatically optimizes your application for the best performance by code splitting, client-side navigation, and prefetching (in production).

Image Component and Image Optimization

next/image is an extension of the HTML <img> element, evolved for the modern web.
source: https://nextjs.org/learn/basics/assets-metadata-css/assets
img css: https://developer.mozilla.org/en-US/docs/Web/CSS/object-fit


Add metadata such as title & favicon tag with Head component from next/head
