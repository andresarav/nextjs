export const get_pairs = async(country, localCurrency = "cop") => {
       let url_get_pair = `https://swap.bitsenda.com/api/pairs?filter={"where":{"secondary_currency.currency":"cop"}}`
       let pairs = await doFetch(url_get_pair)
       // console.log('|||||||||||||||||||||||| pairs:::::', localCurrency, pairs)
       // debugger
       // omitir pares de cotización de bitcoin_tesnet && usd
       return pairs
}


const doFetch = async(url, params) => {
  try {
    const response = await fetch(url, params);
    const finalResponse = await response.json();
    // if (!response.ok && response.status === 465) {
    //   if (finalResponse.error.message.includes("Invalid signature")) {
    //     // TODO: add refresh_token flow to get a new jwt
    //     // doLogout('?message=Invalid signature')
    //   }
    //   throw response.status;
    // }
    return await finalResponse;
  } catch (_) {
    // handleError(_)
    return false;
  }
}
