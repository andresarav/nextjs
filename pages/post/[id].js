import { useEffect } from 'react'
import Layout from '../../components/layout'
import styled from 'styled-components'
import Head from 'next/head'
import { useRouter } from 'next/router'


export default function Post(props) {

  const { query:{ id } } = useRouter()

  useEffect(()=>{
      console.log('|||||||||||||||||| useRouter ', id)
  }, [id])


  return (
      <Layout>
      <Head>
        <title>Profile SEO metadata</title>
        <meta name="description" content="Seo metadata with nex js" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <h1>{id}</h1>
      <TextWrapper>
        Esto es un envoltorio de texto proveido por styled components
      </TextWrapper>
    </Layout>
  )
}

const TextWrapper = styled.p`
  width: auto;
  color: green;
`
