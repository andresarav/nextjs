import { useEffect, useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import Layout, { siteTitle } from '../components/layout'
import utilStyles from '../styles/utils.module.css'
import { usePairs } from '../hooks/usePairs'
import styled from 'styled-components'
// import { getSortedPostsData } from '../lib/posts'
// import { get_pairs } from '../lib/pairs'

// export async function getStaticProps() {
//   const allPostsData = getSortedPostsData()
//   // let pairs = await get_pairs()
//   return {
//     props: {
//       allPostsData
//     }
//   }
// }

export default function Home(props) {
  const [ pairs ] = usePairs()
  console.log('|||||||||||| props ', pairs)

  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <h1 className="title" >
          Go to {' '}
          <Link href="/post/first-post">
            <a> first post! </a>
          </Link>
        </h1>
        <p>[Your Self Introduction]</p>
        <p>
          (This is a sample website - you’ll be building a site like this on{' '}
          <a href="https://nextjs.org/learn">our Next.js tutorial</a>.)
        </p>
      </section>

      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={utilStyles.headingLg}>Blog</h2>
        <ul className={utilStyles.list}>
          {pairs?.map(({ id, buy_pair, buy_price }) => (
            <li className={utilStyles.listItem} key={id}>
              <Link href={`/post/${id}`}>
                <a>{buy_pair}</a>
              </Link>
              <br />
              {buy_price}
              <br />
            </li>
          ))}
        </ul>
      </section>
    </Layout>
  )
}
